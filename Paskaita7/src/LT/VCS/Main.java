/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import static LT.VCS.VcsUtils.*;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Path;

/**
 *
 * @author KorAX
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static final String PVZ_FAILAS = "/Users/KorAX/Desktop/untitled folder/Untitled.txt";
    public static final String PVZ_dir = "/Users/KorAX/Desktop/untitled folder/";
    private static final String UTF_8 = "UTF-8";
    
    private static final String NEW_FILE = "/Users/KorAX/Desktop/untitled folder/file1.txt";
    
    public static void main(String[] args) {
      File pvzFile = new File(PVZ_FAILAS);
      String failoTekstas = "";
      //pvzFile.getPath();
      BufferedReader br = null;
      String line;
      try {
          br = new BufferedReader (
                  new InputStreamReader(new FileInputStream(pvzFile), UTF_8));
          while((line = br.readLine()) != null) {
              failoTekstas += line + System.lineSeparator();
              out(line);
          }
    } catch (Exception e) {
        out(e.getMessage());
    } finally {
          if (br != null) {
              try {
              br.close();
              } catch (Exception e) {
                  out(e.getMessage());
              }
          }
      }
      
      BufferedWriter bw = null;
      try {
          bw = new BufferedWriter (
                  new OutputStreamWriter (new FileOutputStream(pvzFile), UTF_8));
          bw.append(failoTekstas);
          bw.flush();
          
      } catch (Exception e) {
          out(e.getMessage());
      } finally {
          if (bw != null) {
              try {
                  bw.close();
              } catch (Exception e) {
                  out(e.getMessage());
              }
          }
      }
     
      //newFile(inWord("Iveskit file path"),inWord("file name"));
      }
        
        private static File newFile(String dir, String fileName) {
          //return a File object that is created in the given directory
          //dir = PVZ_dir;
          //fileName = "lala";
      
  
          
          File file1 = new File(dir + fileName + ".txt");
          
          if (file1.isDirectory()) {
              if (!file1.exists()){
                  try {
                  file1.mkdirs();
                  } catch (Exception e) {
                      out(e.getMessage());
                  }
              }
              
          try {
          file1.createNewFile();
          } catch (Exception e) {
              out(e.getMessage());
              return null;
          }
          }
          return file1;
    }  
      
}
    

