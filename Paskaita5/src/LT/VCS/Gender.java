/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

/**
 *
 * @author KorAX
 */

 public enum Gender {
    
    MALE("Male", "Vyras",1),
    FEMALE("Female", "Moteris",2),
    OTHER("Other", "Kita",3);
 
    /**
     * @param enLabel the enLabel to set
     */
    public void setEnLabel(String enLabel) {
        this.enLabel = enLabel;
    }

    /**
     * @param ltLabel the ltLabel to set
     */
    public void setLtLabel(String ltLabel) {
        this.ltLabel = ltLabel;
    }
    
    private String enLabel;
    private String ltLabel;
    private int id;
    
    private Gender(String enLabel, String ltLabel, int id) {
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    public static Gender getById(int id){
        for (Gender gen : Gender.values()){
            if (id== gen.getId()){
                return gen;
            }
        }
            return null;
    }
    
    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }
    
}
