/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import static LT.VCS.VcsUtils.*;

/**
 *
 * @author KorAX
 */
public class Player extends Person {
        
    private int dice;

    
    public Player(String name, String email) throws Exception {
        super(name,email);
        //this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
    }
    
        public Player(String name, String email, Gender gender) throws Exception {
        this(name,email);
        this.gender = gender;
        this.dice = random(1, 6);
        //this.gender = gender;
    }
        
        public Player(String name, String email, String surname, Gender gender, int age) throws Exception{
            this(name,email,gender);
            setSurname(surname);
            setAge(age);
        }
    
//    public String getName() {
//        return name;
//    }
        
    
    public int getDice() {
        return dice;
    }
    
    @Override
    public String toString(){
        return super.toString().replaceFirst(")", "") + dice;
    }
}
