/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import java.io.IOException;

/**
 *
 * @author KorAX
 */
public class Person {
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    private String email;
    
public Person(String name, String email) throws BadDataInputException, OffensiveDataInputException{
    
    this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    if (email==null || email.trim().length() == 0 || email.equals("vardenis")){
        throw new BadDataInputException("E-mail must not be null");
    } else {
    this.email = email;
    }
}

public Person(String name, String email, Gender gender) throws Exception{
    this(name,email);
    this.gender = gender;
}

public Person(String name, String surname, Gender gender, int age, String email) throws Exception{
    this(name,email,gender);
    this.age=age;
    this.surname=surname;
}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    @Override
    public String toString(){
        return "Player(name)" + name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();

    }

    public String getEmail() {
        return email;
    }
}
