/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;
import static LT.VCS.VcsUtils.*;
/**
 *
 * @author KorAX
 */
public class Player {
    
           
           private String name;
    private int dice;
    
    public Player(String name) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
    }
    
    public String getName() {
        return name;
    }

    public int getDice() {
        return dice;
    }

        
    
}
