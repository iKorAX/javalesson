/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

/**
 *
 * @author KorAX
 */
//public class Gender {
    
    public enum Gender {
    
    MALE("Male", "Vyras"),
    FEMALE("Female", "Moteris"),
    OTHER("Other", "Kita");
    
    private String enLabel;
    private String ltLabel;
    
    private Gender(String enLabel, String ltLabel) {
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
    }

    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }
    
}


