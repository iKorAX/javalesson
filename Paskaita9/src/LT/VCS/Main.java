/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import static LT.VCS.VcsUtils.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author KorAX
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // url points to jdbc protocol : mysql subprotocol; localhost is the address
        // of the server where we installed our DBMS (i.e. on local machine) and
        // 3306 is the port on which we need to contact our DBMS
        String url = "jdbc:mysql://localhost:3306/";
        String driver = "com.mysql.jdbc.Driver";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Hurray! We've connected to the database! " + conn.getSchema());
            Statement n = conn.createStatement();
            
            n.executeUpdate("insert into person values (" + (getLastId(n, "person")+1) + ",'As','Jis','Other',77,'as@mano.lt')");
            //conn.commit(); 
            ResultSet rs = n.executeQuery("select * from person;");
            //out(rs);
            //rs.absolute(0);
            while (rs.next()) {
                //rs.getString("name");
                //rs.getString(1);
                
                String name = rs.getString("name");
                String email = rs.getString("email");
                String gender = rs.getString("gender");
                Integer id = rs.getInt("id");
                out("name: " + name + "\t email: " + email + "\t id: " + id + " \t gender: " + gender);
                out("name: " + rs.getMetaData().getColumnName(2) + "; type: " + rs.getMetaData().getColumnType(2));
                //out("the last id is" + getLastId(n,"person"));
            }

        } catch (Exception e) {
            out("Could not connect to the database. " + e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
           
            
        }
    }
     private static Integer getLastId(Statement n, String table) {
                Integer result = null;
                try {
                ResultSet rs = n.executeQuery("SELECT id FROM " + table + " ORDER BY id DESC LIMIT 1;");
                if(rs.next()) {
                    result = rs.getInt(1);
                }
                
                } catch (Exception e) {
                    out(e.getMessage());
                }
                
                return result;
            }
}
