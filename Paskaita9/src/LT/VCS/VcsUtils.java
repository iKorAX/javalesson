/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author KorAX
 */
public class VcsUtils {
    public static String inWord(){
        return newScan().next();
    }
    
    public static String inWord(String txt){
        out(txt);
        return inWord();
    }
    
    public static String inPath(String txt){
        out(txt);
        return inWord();
    }
    
    private static Scanner newScan() {
        return new Scanner(System.in);
    }

    public static int inInt() {
        return newScan().nextInt();
    }
    
    public static int inInt(String txt) {
        out(txt);
        return inInt();
    }
    
    
    public static void out(Object text){
        System.out.print(timeNow()+" "+ text);
          System.out.println();
    }
    public static String timeNow(){
        SimpleDateFormat sdf1 = new SimpleDateFormat("'['HH:mm:ss:SS']'");
        return sdf1.format(new Date());
    }
    public static String inLine(){
        return newScan().nextLine();
    }
    
        public static String inLine(String txt){
        out(txt);
        return inLine();
    }
    
    public static int random(int from, int to){
       
        return ThreadLocalRandom.current().nextInt(from, to +1);

    }
    
//private variable (int) that would be named Dice and hold
    
    public static void Lecture33(){
    String ghj = "bla bla";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.DD HH:mm:ss");
        Date date = new Date();
        out(ghj);
    }
}
