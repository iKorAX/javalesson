/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LT.VCS;

import java.util.Scanner;

/**
 *
 * @author KorAX
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Exercise2 number = new Exercise2();
        Exercise3 calculator = new Exercise3();
        Exercise4 symbolnumber = new Exercise4();
        Exercise5 cyclecalculator = new Exercise5();
        Exercise6 multiplicationtable = new Exercise6();
        
        Scanner sc = new Scanner(System.in);
        int exerciseNumberArray[] = new int[]{2,3,4,5,6,7};

        System.out.println("Input the number of the exercise (2 to 7:");
        int exerciseNumber = sc.nextInt();
        System.out.println("You have typed number " + exerciseNumber);

        
        if (exerciseNumber ==2)
        {
             number.numberOneToTen();
        } else if (exerciseNumber ==3)
        {
            calculator.SimpleCalculator();
        } else if (exerciseNumber ==4)
        {
            symbolnumber.SymbolCalculator();
        } else if (exerciseNumber ==5)
        {
            cyclecalculator.CycleCalculator();
        } else if (exerciseNumber ==6)
        {
            multiplicationtable.MultiplicationTable();
        }
        
        
    }
    
   
    
}
